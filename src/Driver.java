/**
 * This class was written to test the SaleItem class.
 * 
 * @author Justin E. Ervin
 * @version 2-20-2014
 */

import java.util.Scanner;

public class Driver {

	/**
	 * Execution of this program starts in the main( ) method
	 * 
	 * @param Java arguments
	 */
	public static void main(String[] args) {
		SaleItem myItem; // A reference variable for SaleItem
		Scanner input; // A reference variable for Scanner
		double originalPrice; // Stores the original price of the item from the user input
		double cashTendered; // Stores the cash tendered from the user input
		double discountPercent; // Stores the percent of discount on the item in percent form
		double startingDiscount;  // Stores the starting percent of discount on the item in percent form
		double taxRate; // Stores the percent of tax on the item in percent form
		char isDone; // Stores whether or not we ask the user for another item.
		int count = 0; // Stores the number of times the loop executes

		// Create an instance using the Scanner class to get input from the keyboard
		input = new Scanner(System.in);
		
		// Prompt the user for the original price of the item and validate it
		do {
			System.out.println("Enter the price of the item: ");
			originalPrice = input.nextDouble();
		} while (originalPrice < 0);
		
		// Prompt the user for percent of discount on the item and validate it
		do {
			System.out.println("Enter the percent of discount on the item: ");
			startingDiscount = input.nextDouble();
		} while (startingDiscount < 0);
		
		// Set the starting percent of discount on the item
		discountPercent = startingDiscount;
		
		// Prompt the user for percent of tax on the item and validate it
		do {
			System.out.println("Enter the percent of tax on the item: ");
			taxRate = input.nextDouble();
		} while (taxRate < 0);
		
		// Create an instance using the SaleItem class
		myItem = new SaleItem(originalPrice, startingDiscount, taxRate);

		do {
			if (count != 0) {
				// Reset values of myItem instance
				myItem.setPrice(0);
				myItem.setDiscountPercent(startingDiscount);

				// Prompt the user for the original price of the item and validate it
				do {
					System.out.println("Enter the price of the item: ");
					originalPrice = input.nextDouble();
				} while (originalPrice < 0);

				// Set the original price for myItem instance
				myItem.setPrice(originalPrice);
			}
			
			// Prompt the user for how much cash was tendered and validate it
			do {
				System.out.println("Enter how much cash was tendered: ");
				cashTendered = input.nextDouble();
			} while (cashTendered < 0);

			// Print the values of the different prices calculated from myItem object
			System.out.println("\nOriginal Price: $" + originalPrice);
			System.out.println("Discount Price: $" + myItem.calcDiscountPrice());
			System.out.println("Final Price: $" + myItem.calcFinalPrice());
			System.out.println("Change Due: $" + (cashTendered - myItem.calcFinalPrice()));

			// Prompt the user for percent of discount on the item and validate it
			do {
				System.out.println("\nEnter the percent of discount on the item: ");
				discountPercent = input.nextDouble();
			} while (discountPercent < 0);

			// Set the percent of discount for myItem instance
			myItem.setDiscountPercent(discountPercent);

			// Prompt the user for how much cash was tendered and validate it
			do {
				System.out.println("Enter how much cash was tendered: ");
				cashTendered = input.nextDouble();
			} while (cashTendered < 0);

			// Print the values of the different prices calculated from myItem object
			System.out.println("\nOriginal Price: $" + originalPrice);
			System.out.println("Discount Price: $" + myItem.calcDiscountPrice());
			System.out.println("Final Price: $" + myItem.calcFinalPrice());
			System.out.println("Change Due: $" + (cashTendered - myItem.calcFinalPrice()));
			
			// Update loop count
			count++;
			
			// Prompt the user if they want to enter another item
			do {
				System.out.println("\nDo you want to enter another item (y = yes, n = no): ");
				isDone = input.next().charAt(0);
			} while (isDone != 'Y' && isDone != 'y' && isDone != 'N' && isDone != 'n');
		} while (isDone == 'Y' || isDone == 'y');

		// Close the Scanner class that was getting input from the keyboard
		input.close();
	}
}