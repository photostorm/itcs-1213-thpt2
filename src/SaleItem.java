/**
 * This class holds a price of item and calculates both discount price and final price
 * 
 * @author Justin E. Ervin
 * @version 2-20-2014
 */

public class SaleItem {
	// Declare all constants variables
	private final double TAX_RATE; // Stores the percent of tax on the item in decimal form

	// Declares all needed variables
	private double priceOfItem; // Stores the price of the item
	private double discountPercent; // Stores the percent of discount on the item in decimal form

	/**
	 * Constructor for objects of class CurrencyConverter
	 */
	public SaleItem() {
		priceOfItem = 0;
		discountPercent = 10 / 100;
		TAX_RATE = 7.5 / 100;
	}

	/**
	 * Constructor for objects of class CurrencyConverter
	 * 
	 * @param the price of the item
	 * @param the percent of discount on the item
	 * @param the percent of tax on the item
	 */
	public SaleItem(double price, double discount, double rate) {
		priceOfItem = price;
		discountPercent = discount / 100;
		TAX_RATE = rate / 100;
	}

	/**
	 * This method allows the user to change the price of the item
	 * 
	 * @param the price of the item
	 */
	public void setPrice(double price) {
		priceOfItem = price;
	}

	/**
	 * This method allows the user to change the percent of discount on the item and convert the
	 * percent to correct form
	 * 
	 * @param the percent of discount on the item
	 */
	public void setDiscountPercent(double discount) {
		discountPercent = discount / 100;
	}

	/**
	 * This method calculates the discount price of the item and return it.
	 * 
	 * @return the discount price of the item
	 */
	public double calcDiscountPrice() {
		return (priceOfItem - (priceOfItem * discountPercent));
	}

	/**
	 * This method calculates the final price of the item and return it.
	 * 
	 * @return the final price of the item
	 */
	public double calcFinalPrice() {
		return calcDiscountPrice() + (calcDiscountPrice() * TAX_RATE);
	}
}
